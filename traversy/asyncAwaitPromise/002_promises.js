/* 
BEGIN SIMULATE SERVER
*/
const posts = [
    { title: 'Post One', body: 'this is post one' },
    { title: 'Post Two', body: 'this is post two' }
];

/* 
our server returned posts in 1 second
*/
const getPosts = () => {
    setTimeout(() => {
        let output = '';
        posts.forEach((post, index) => {
            output += `<li>${post.title}</li>`;
        });
        document.body.innerHTML = output;
    }, 1000);
}

const createPostWithPromise = post => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            posts.push(post);

            const error = false;

            if (!error) {
                resolve();
            } else {
                /* 
                if error is set to true it is going to reject 

                and we get an uncaught promise/error
                */
                reject('Error: Something went wrong');
            }
         }, 2000);
    })
}
/* 
END SIMULATE SERVER
*/
  
/* 
createPostWithCallback returns promise MEANING THAT we can now USE .then()

createPostWithPromise is setting the timeout and then as soon as timeout is done createPostWithPromise is going to resolve

once createPostWithPromise resolves then createPostWithPromise call getPosts
*/
createPostWithCallback({ title: 'Post Three', body: 'this is post three' })
    .then(getPosts)
    .catch(err => console.log(err));
