/* 
because if you have a lot of different promises 
you dont want to keep having .then() .then() .then()

so you can use promise.all
*/

/* 
BEGIN SIMULATE SERVER
*/
const posts = [
    { title: 'Post One', body: 'this is post one' },
    { title: 'Post Two', body: 'this is post two' }
];

const getPosts = () => {
    setTimeout(() => {
        let output = '';
        posts.forEach((post, index) => {
            output += `<li>${post.title}</li>`;
        });
        document.body.innerHTML = output;
    }, 1000);
}

const createPostWithPromise = post => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            posts.push(post);

            const error = false;

            if (!error) {
                resolve();
            } else {
                reject('Error: Something went wrong');
            }
        }, 2000);
    })
}
/* 
END SIMULATE SERVER
*/

const promise1 = Promise.resolve('Hello World');
const promise2 = 10;
const promise3 = new Promise((resolve, reject) => setTimeout(resolve, 2000, 'Goodbye'));
const promise4 = fetch('https://jsonplaceholder.typicode.com/users')
    .then(res => res.json());

/*
Promise.all is going to take long the longest promise is that is how long it is going to take the actually show us the values
*/
Promise.all([promise1, promise2, promise3, promise4])
    .then(values => console.log(values));