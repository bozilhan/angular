/* 
async await is a way to handle responses it is not like a different way to write then or anything 

basically we need to have function that is labelled `async` 

it is JUST MORE ELEGANT WAY to HANDLE PROMISES

it is just a cleaner way of dealing with promises INSTEAD OF DOING .then()
*/

/* 
BEGIN SIMULATE SERVER
*/
const posts = [
    { title: 'Post One', body: 'this is post one' },
    { title: 'Post Two', body: 'this is post two' }
];

const getPosts = () => {
    setTimeout(() => {
        let output = '';
        posts.forEach((post, index) => {
            output += `<li>${post.title}</li>`;
        });
        document.body.innerHTML = output;
    }, 1000);
}

const createPostWithPromise = post => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            posts.push(post);

            const error = false;

            if (!error) {
                resolve();
            } else {
                reject('Error: Something went wrong');
            }
        }, 2000);
    })
}
/* 
END SIMULATE SERVER
*/

/* 
ASYNC AWAIT

init function has to be labelled `async` if we want to use `await` inside of it

`await` WAITS FOR an asynchronous process or action TO COMPLETE
*/
const init = async () => {
    /* 
    awaiting createPostWithCallback to be done until we move on and call  getPosts();
    */
    await createPostWithCallback({ title: 'Post Three', body: 'this is post three' })

    getPosts();
};

init();


