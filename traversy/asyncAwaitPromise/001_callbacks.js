/* 
BEGIN SIMULATE SERVER
*/
const posts = [
    { title: 'Post One', body: 'this is post one' },
    { title: 'Post Two', body: 'this is post two' }
];

/* 
our server returned posts in 1 second
*/
const getPosts = () => {
    setTimeout(() => {
        let output = '';
        posts.forEach((post, index) => {
            output += `<li>${post.title}</li>`;
        });
        document.body.innerHTML = output;
    }, 1000);
}

/* 
createPost however took 2 seconds
*/
const createPost = post => {
    setTimeout(() => {
        posts.push(post);
    }, 2000);
}
/* 
END SIMULATE SERVER
*/


getPosts();

/* 
createPost took longer than the getPost therefore we list just post1 and post2

by the time we ran createPost the dom is already painted so we cant do anything beyond that point

this is where asynchronous programming comes in and this is where callbacks come in which is one way to handle it
*/
createPost({ title: 'Post Three', body: 'this is post three' })


const createPostWithCallback = (post, callback) => {
    setTimeout(() => {
        posts.push(post);
        
        /*
        we want that function to be called right after `posts.push(post)` not waiting 2 seconds for the entire function
        */
        callback();
    }, 2000);
}

/* 
getPosts with NO PARENTHESIS
*/
createPostWithCallback({ title: 'Post Three', body: 'this is post three' }, getPosts);
