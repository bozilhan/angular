/* 
async await fetch
*/
const fetchUsers = async () => {
    const response = await fetch('https://jsonplaceholder.typicode.com/users');

    const data = await response.json();

    console.log(data)
};

fetchUsers(); 