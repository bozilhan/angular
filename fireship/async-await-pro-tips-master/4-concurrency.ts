import { getFruit } from './3-async-await';

/* 
so we know that an async function always returns a promise so INSTEAD OF doing one after the other we can add BOTH OF OUR PROMISES to promise.all() 

promise.all() will tell all the promises in the array to RUN CONCURRENTLY and then have the resolved values be at that index in the array

you should always be thinking about when working with async functions you dont want to accidentally pause function unnecessarily

so instead of awaiting a whole bunch of individual promises you might want to add all your promises to an array and then await that promise.all() 

as you can see here we have DOUBLED the speed of the original function
*/
const makeSmoothieFaster = async() => {
    const a = getFruit('pineapple');
    const b = getFruit('strawberry');

    const smoothie = await Promise.all([a, b])

    return smoothie;
}


const fruitRace = async() => {
    const a = getFruit('pineapple');
    const b = getFruit('strawberry');

    const winner = await Promise.race([a, b])

    return winner;
}

// fruitRace().then(log)
// fruitRace().then(log)
// fruitRace().then(log)
// fruitRace().then(log)
// fruitRace().then(log)
