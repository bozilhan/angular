import { getFruit } from './3-async-await';

/* 
let's imagine we have a string of IDs and then we want to retrieve all these ids from the database
*/
const fruits = ['peach', 'pineapple', 'strawberry'];

/* 
we can use array.map() to convert them to an array of promises and then resolve them all CONCURRENTLY using promise.all()

you need to be careful when using async/await in a map or forEach because it wont actually pause the function in this context 

so normally we would expect this loop to stop if we do await getFruit() but that is actually NOT what happens in this case

instead of we will run all these promises concurrently so that might not be the behavior that you are expecting

if you want to run a loop and have every iteration in that loop await to promise you need to use a TRADITIONAL for loop

so you can write async functions and then write a for loop inside that function and then use the `await` keyword inside the loop 

when you write your code like this it will pause each step of a loop until that promise is resolved
*/
const smoothie = fruits.map(async v => {
  const emoji = await getFruit(v);
  log(emoji);
  return emoji;
});

/* 
but more often than not(cogu zaman, sik sik) you will probably want to run everything concurrently and a cool thing you can do is use the `await` keyword directly in a for loop 

if you have a promise that you know results to an array you can actually just use `await` keyword directly in your loop so you can say for await in your code which will await the array of items to resolve and then loop over them immediately after
*/
const smoothie2 = fruits.map(v => getFruit(v));

const fruitLoopAwaitDirectly = async () => {
  for await (const emoji of smoothie2) {
    log(emoji);
  }
};

const fruitLoop = async () => {
  for (const f of fruits) {
    const emoji = await getFruit(f);
    log(emoji);
  }
};

/* 
you can also use the `await` keyword in your conditionals on the left side of the conditional we can await the result value from promise and then we can see if it is equal to some other value 

so that gives you super concise way to write conditional expressions when working with promises 
*/
const fruitInspection = async () => {
  if ((await getFruit('peach')) === '🍑') {
    console.log('looks peachy!');
  }
};

import fetch from 'node-fetch';

const getTodo = async () => {
  const res = await fetch('https://jsonplaceholder.typicode.com/todos/1');

  const { title, userId, body } = await res.json();

  console.log(title, userId, body);
};
