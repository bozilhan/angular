import fetch from 'node-fetch';

/* 
fetch allows us to hit an http endpoint and have the response returned to us as a PROMISE of the response  

fetching data from a remote server is always going to be async 
so we can queue up the promise then provide it with a callback to map it to JSON 
*/

const promise = fetch('https://jsonplaceholder.typicode.com/todos/1');

promise
  /* 
  the great thing about promises is that we can chain them together 

  map in to JSON is also a promise so we can return that promise from the original then() callback and then in the next one we will have the actual user data as a plain js object
  */
  .then(res => res.json())
  .then(todo => console.log('😛', todo.title));

console.log('🥪 Synchronous');

/*  
if we run this code you will see it runs our console.log('🥪 Synchronous'); first and it retrieves the data from the API and console.log('😛', todo.title) that afterwards
*/

/* 
the great thing about promises is that you can catch all errors in the chain with a single function we can do this by adding catch to the bottom of our promise chain and it will handle errors that happen anywhere within our asynchronous code 

if this code were callback based we'd have to have a separate error handler for every single one of the asynchronous operations so if an error is thrown anywhere in our code it is going to bypass all of the future then callbacks and go straight to the catch callback 
*/
promise
  .then(res => res.json())
  .then(todo => {
    throw new Error('uh oh');
    return todo;
  })
  .then(todo => console.log('😛', todo.title))
  .catch(err => console.error('😭', err));

console.log('🥪 Synchronous');
