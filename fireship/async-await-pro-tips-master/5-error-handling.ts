 import { getFruit } from './3-async-await';

/* 
instead of chaining a catch callback to our promise chain we can just 
wrap our code in a try/catch block 

this offers much better flexibility when handling errors that might occur across multiple promises
*/ 
const badSmoothie = async() => {
    try {

        const a = getFruit('pineapple')
        const b = getFruit('strawberry');
        const smoothie = await Promise.all([a, b])

        throw 'broken!'

        return smoothie;

    } catch (err) {
        /* 
        we can either catch the error and throw another error 
        or we can catch the error and return a value
        */
        console.log(err)
        // return `😬 We are going to be fine...`
        throw `💩 It's broken!`
    }
}