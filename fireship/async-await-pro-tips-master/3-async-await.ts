/* 
we are already know that promises are a huge improvement over callbacks but promises can still be really hard to read and follow especially when you have a long chain of multiple asynchronous events

async await really just boils down to SYNTACTIC SUGAR TO MAKE YOUR ASYNCHRONOUS CODE READ LIKE SYNCHRONOUS CODE
*/

/* 
if we put the `async` keyword in front of regular function we have a function that RETURNS A PROMISE

so whatever gets returned inside this function will be a promise of that value
*/
export const getFruit = async name => {
  /* 
  promise based API simulation

  in this case the user can pass in the name of a fruit and then the function will resolve to the value of the fruit emoji from this object
  */
  const fruits = {
    pineapple: '🍍',
    peach: '🍑',
    strawberry: '🍓'
  };

  /* 
  when you use the `async` keyword `async` takes the return value and AUTOMATICALLY resolves it as a promise
  */
  return fruits[name];
};

/* 
if we didnt use the `async` keyword we could write this function by just returning a promise that resolves to this value
*/
const getFruitWithoutAsyncKeyword = name => {
  const fruits = {
    pineapple: '🍍',
    peach: '🍑',
    strawberry: '🍓'
  };

  return Promise.resolve(fruits[name]);
};

getFruit('peach').then(console.log);

/* 
the real power of an async function comes when you combine it with the `await` keyword
TO PAUSE THE EXECUTION of the function
*/
export const makeSmoothie = async () => {
  /* 
  we need to do is get multiple fruits and then combine them together as a single value

  INSTEAD OF CHAINING together a bunch of then callbacks we can just have a promise resolve to the value of a variable

  `await` is like saying PAUSE the EXECUTION of this function UNTIL THE getFruit() promise resolves to a value at which point we can use it as the variable `a`

  and then we will move on to the next line of code

  after we get a pineapple we can then get a strawberry and then we will turn them together as an array



  !!!!!
  you really only need to `await` one thing after the other 
  IF THE SECOND VALUE IS DEPENDENT ON THE FIRST VALUE

  for example if you need to get a userId before you can then retrieve(geri almak) some data from the database

  lets imagine we are making these calls from a remote API and there is about a second of latency

  if we run this code again with the delay you can see it takes a full second to get the first fruit and then a full second to get the second fruit 

  but whole point of the event loop is to avoid blocking code like this
  !!!
  */
  const a = await getFruit('pineapple');
  const b = await getFruit('strawberry');

  return [a, b];
};

/* 
one of the most annoying things with promises is that it is kind of difficult to share result values between multiple steps in the promise chain

async/await solves this problem really nicely 
*/
const makeSmoothieWithoutAsyncAwait = () => {
  let a;
  return getFruit('pineapple')
    .then(v => {
      a = v;
      return getFruit('strawberry');
    })
    .then(v => [a, v]);
};
