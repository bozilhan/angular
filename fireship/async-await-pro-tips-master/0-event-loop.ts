/* 
HOW EVENT LOOP DOES WORK

in order to understand anything async you really need to first understand the EVENT LOOP

WHAT IS EVENT LOOP WHAT DOES IT HAVE TO DO WITH ASYNC WAY
both the browser and nodejs are always running a single threaded EVENT LOOP to run your code 
on the first go round it will run all of your synchronous code but event loop might also queue up(siraya sokmak) asynchronous events to be called back later 

getData(callBack) 
you say here is a function that I need to run but first I need to go get some data from the network. event loop says OK I will keep doing my thing while you do your thing in a separate thread pool. then at some point in the future getData() will finish and let the event loop know that it is ready to be called back

if it is a macro task like a setTimeout() or setInterval() it will be executed on the NEXT EVENT LOOP 
if it is a MICRO task like a fulfilled(yerine getirilmis) promise then it will be called back BEFORE the START of the next event loop 
*/


// L1
console.log('🥪 Synchronous 1');

// L2
setTimeout(_ => console.log(`🍅 Timeout 2`), 0);

// L3
Promise.resolve().then(_ => console.log('🍍 Promise 3'));

// L4
console.log('🥪 Synchronous 4');


/* 
if you execute this code you can see that console.log('🥪 Synchronous 1'); gets logged right away(derhal,hemen,aninda) because it is running on the main thread

if we run the setTimeout(_ => console.log(`🍅 Timeout 2`), 0); it is being queued for a future task 

then the Promise.resolve().then(_ => console.log('🍍 Promise 3')); is being queued to run in the micro task queue immediately after this current task 

and finally console.log('🥪 Synchronous 4'); gets executed right away 

so even though the setTimeout callback was queued up before the promise the promise still gets executed first because of the priority of the micro task queue 

OUTPUT
synchronous 1
synchronous 4
Promise 3
Timeout 2
*/