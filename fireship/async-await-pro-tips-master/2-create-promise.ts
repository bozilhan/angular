
const tick = Date.now();
const log = v => console.log(`${v}\n Elapsed: ${Date.now() - tick}`);

const codeBlocker = () => {
  /* 
  if we run this on the main thread it is going to block all of their code from executing 
  until the billion loops are done
  */
  let i = 0;
  while (i < 10000000000) {
    i++;
  }

  return 'Billion loops done';
};

log('🥪 Synchronous 1');

log(codeBlocker);

log('🥪 Synchronous 2');

const codeBlockerWrappedPromise = () => {
  /* 
  we can get codeBlockerWrappedPromise off the main thread and execute codeBlockerWrappedPromise AS A MICRO TASK

  we create a new promise we add our code inside that promise and then we have it resolved to that value when done
  */
  return new Promise((resolve, reject) => {
    /* 
    so you might think that because we are wrapping this code in a promise 
    that we are going to execute this off the main thread

    but the actual creation of the promise and that big while loop is
    STILL HAPPENING ON THE MAIN THREAD

    it is only resolving the value that happens as a micro task
    */
    let i = 0;
    while (i < 10000000000) {
      i++;
    }

    resolve('Billion loops done');
  });
};
/* 
OUTPUT
🥪 Synchronous 1 Elapsed: 0ms
🥪 Synchronous 2 Elapsed: 730ms
Billion loops done Elapsed: 731ms

codeBlockerWrappedPromise's while loop is still blocking on the main thread
*/

/* 
to ensure that all of our synchronous code runs as fast as possible 
we will refactor our code once again to say promise.resolve() then we will run the the while loop INSIDE OF THAT RESOLVED PROMISES CALLBACK

by putting this code inside of a resolved promise we CAN BE GUARANTEED that it will be executed AFTER ALL the synchronous code in the CURRENT MACRO TASK HAS COMPLETED
*/
const notCodeBlocker = () => {
  return Promise.resolve().then(v => {
    let i = 0;
    while (i < 10000000000) {
      i++;
    }
    return 'Billion loops done';
  });
};
/* 
OUTPUT
🥪 Synchronous 1 Elapsed: 0ms
🥪 Synchronous 2 Elapsed: 4ms
Billion loops done Elapsed: 719ms
*/