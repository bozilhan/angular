import {Component, OnInit} from '@angular/core';

@Component({
    selector: 'app-board',
    templateUrl: './board.component.html',
    styleUrls: ['./board.component.scss']
})
export class BoardComponent implements OnInit {
    // nine moves on the game board
    squares!: any[];

    // to help us determine the current player
    xIsNext!: boolean;

    // winner will either be X or O
    winner!: string;

    constructor() { }

    ngOnInit(): void {
        this.newGame();
    }

    newGame() {
        this.squares = Array(9).fill(null);
        this.xIsNext = true;
    }

    get player() {
        return this.xIsNext ? 'X' : 'O';
    }

    /*
    * it will serve as event handler for when the user clicks on one of the buttons
    * to make a move when the click event happens we will check the index in the array
    * that they clicked on
    * if that square has already been clicked then we wont do anything
    * */
    makeMovie(idx: number) {
        if (!this.squares[idx]) {
            this.squares.splice(idx, 1, this.player);
            this.xIsNext = !this.xIsNext;

        }

        this.winner = this.calculateWinner();
    }

    calculateWinner() {
        const lines = [
            [0, 1, 2],
            [3, 4, 5],
            [6, 7, 8],
            [0, 3, 6],
            [1, 4, 7],
            [2, 5, 8],
            [0, 4, 8],
            [2, 4, 6]
        ];

        for (let i = 0; i < lines.length; i++) {
            const [a, b, c] = lines[i];
            if (
                this.squares[a] &&
                this.squares[a] === this.squares[b] &&
                this.squares[a] === this.squares[c]
            ) {
                return this.squares[a];
            }
        }
        return '';
    }
}

