import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-dialog-board',
  template: `
    <h1 mat-dialog-title>Board</h1>
    <div mat-dialog-content>
      <p>what shall we call this board?</p>
      <mat-form-field>
        <input placeholder="title" matInput [(ngModel)]="data.title"/>
      </mat-form-field>
    </div>
    <div mat-mat-dialog-actions>
      <button mat-button (click)="onNoClick()"> Cancel</button>
      <button
      mat-button
      [mat-mat-dialog-close]="data.title"
      cdkFocusInitial
      (click)="onClick()"> Create</button>
    </div>
  `,
  styles: []
})
export class DialogBoardComponent {

  constructor(
    public dialogRef: MatDialogRef<DialogBoardComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  onNoClick() {
    this.dialogRef.close();
  }
}
