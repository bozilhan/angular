import { CdkDragDrop } from '@angular/cdk/drag-drop';
import { ThrowStmt } from '@angular/compiler';
import { Component, Input, OnInit } from '@angular/core';
import { BoardService } from '../board.service';

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.scss']
})
export class BoardComponent  {
  @Input() board;

  constructor(private boardService: BoardService) { }

  taskDrop(event: CdkDragDrop<string[]>) {
    this.boardService.updateTasks(this.board.id, this.board.tasks);

  }
}
