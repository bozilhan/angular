import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { BoardService } from '../board.service';
import { DialogBoardComponent } from '../dialogs/dialog-board.component';

@Component({
  selector: 'app-board-list',
  templateUrl: './board-list.component.html',
  styleUrls: ['./board-list.component.scss']
})
export class BoardListComponent implements OnInit, OnDestroy {
  /*
  create manual subscription to an observable directly in the component
  when you do that you also want to make sure that you DISPOSE the subscription
  when this component is destroyed
  */

  boards: Board[];

  /*
  subscription to the observable
  */
  sub: Subscription;

  constructor(public boardService: BoardService, public dialog: MatDialog ) { }

  ngOnInit() {
    this.sub = this.boardService
      .getUserBoards()
      .subscribe(boards => this.boards = boards);
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  drop(event: CdkDragDrop<string[]>) {

    moveItemInArray(this.boards, event.previousIndex, event.currentIndex)
    this.boardService.sortBoards(this.boards);
  }

  openBoardDialog() {
    const dialogRef = this.dialog.open(DialogBoardComponent, {
      width: '400px',
      data:{}
    });

    dialogRef.afterClosed().subscribe(result => {
       if (result) {
         this.boardService.createBoard({
           title: result,
           priority: this.boards.length;
})
       }
     })
  }

}
