import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { Board } from './board.model';
import firebase from 'firebase';
import DocumentReference = firebase.firestore.DocumentReference;
import { Task } from './task.model';
import FieldValue = firebase.firestore.FieldValue;
import { switchMap } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class BoardService {

    constructor(private afAuth: AngularFireAuth, private db: AngularFirestore) { }

    /*
    * Creates a new board for the current user
    * */
    async createBoard(data: Board): Promise<DocumentReference<{}>> {
        const user = await this.afAuth.auth.currentUser;

        return this.db.collection('boards').add({
            ...data,
            uid: user.uid,
            tasks: [{description: 'Hello!', label: 'yellow'}]
        });
    }

    deleteBoard(boardId: string): Promise<void> {
        return this.db
            .collection('boards')
            .doc(boardId)
            .delete();
    }

    updateTasks(boardId: string, tasks: Task[]): Promise<void> {
        return this.db.collection('boards')
            .doc(boardId)
            .update({tasks});
    }

    removeTask(boardId: string, task: Task): Promise<void> {
        return this.db
            .collection('boards')
            .doc(boardId)
            .update({
                tasks: FieldValue.arrayRemove(task)
            });
    }

    getUserBoards(): Observable<any[] | Observable<(Board & { id: string })[]>> {
        return this.afAuth.authState.pipe(
            /*
            * instead of getting the current user as a promise
            * we will get the current user's authState as an observable
            * with that observable we can then pipe in swithMap operator
            * */
            switchMap(user => {
                if (user) {
                    return this.db
                        .collection<Board>('boards', ref =>
                            ref.where('uid', '==', user.uid)
                                .orderBy('priority')
                        )
                        .valueChanges({idField: 'id'});
                } else {
                    return [];
                }
            })
        );
    }

    /*
    * run a batch write to change the priority of each board for sorting
    * */
    sortBoards(boards: Board[]): void {
        const db = firebase.firestore();
        const batch = db.batch();
        const refs = boards.map(b => db.collection('boards').doc(b.id));
        refs.forEach((ref, idx) => batch.update(ref, {priority: idx}));
        batch.commit().then(r => {});
    }
}
