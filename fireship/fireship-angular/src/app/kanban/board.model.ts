import { Task } from './task.model';

/*
* add a ? for each property which makes them optional properties
* because in some case you want to write partial amount of data
* and in some cases the id wont exist when you are first writing data to the database
* */
export interface Board {
    id?: string;
    title?: string;
    priority?: number;
    tasks?: Task[];
}
