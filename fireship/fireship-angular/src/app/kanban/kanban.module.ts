import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { KanbanRoutingModule } from './kanban-routing.module';
import { SharedModule } from '../shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { BoardListComponent } from './board-list/board-list.component';
import { BoardComponent } from './board/board.component';
import { DialogBoardComponent } from './dialogs/dialog-board.component';
import { DialogTaskComponent } from './dialogs/dialog-task.component';


@NgModule({
    declarations: [BoardListComponent, BoardComponent, DialogBoardComponent, DialogTaskComponent],
    imports: [
        CommonModule,
        KanbanRoutingModule,
        SharedModule,
        FormsModule,
        DragDropModule,
        MatDialogModule,
        MatButtonToggleModule
    ],
    entryComponents: [DialogBoardComponent, DialogTaskComponent]
})
export class KanbanModule {
}
