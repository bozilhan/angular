export interface Task {
    description?: string;
    /*
    * UNION TYPE of typescript
    * by separating different string values with a |
    *
    * you know exactly which values are valid
    * */
    label?: 'purple' | 'blue' | 'green' | 'yellow' | 'red' | 'gray';
}
