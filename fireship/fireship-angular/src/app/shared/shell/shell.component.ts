import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';

@Component({
  selector: 'app-shell',
  templateUrl: './shell.component.html',
  styleUrls: ['./shell.component.scss'],
})
export class ShellComponent {
  isHandset$:Observable<boolean> = this.breakpointObserver.observe([Breakpoints.Handset])
  .pipe(
    map(result => result.matches),
    shareReplay()
  );

  /*
  BreakpointObserver listens to different breakpoints
  depending on how you want to react to different view port sizes

  Handset breakpoint is useful when if you are targeting desktop or mobile
  */
  constructor(private breakpointObserver:BreakpointObserver) {}

  ngOnInit(): void {}
}
