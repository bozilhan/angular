import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-delete-button',
  templateUrl: './delete-button.component.html',
  styleUrls: ['./delete-button.component.scss']
})
export class DeleteButtonComponent implements OnInit {

  /*
  generic button that is able to emit a custom event when user has confirmed deletion of whatever object
  */
  constructor() { }

  ngOnInit(): void {
  }

}
