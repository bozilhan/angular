import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss'],
})
export class LoginPageComponent implements OnInit {

  /*
  when injecting a service using public keyword instead of the private keyword
  allows us to also use the service in the template(xxx.html)
  */
  constructor(public afAuth: AngularFireAuth) {}

  ngOnInit(): void {}
}
