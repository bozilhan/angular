import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { SnackService } from '../services/snack.service';

/*
* the purpose of this particular guard would be to block access to certain routes
*
* we can use this guard to block routes
* but it is generally not useful
* if you dont show the user some kind of a useful notification why the route has been blocked
* */

@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanActivate {
    constructor(private afAuth: AngularFireAuth, private snackService: SnackService) {}

    async canActivate(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Promise<boolean> {

        /*
        * if the user is logged in then await this.afAuth.auth.currentUser; will give us `user` object
        * but if not logged in  await this.afAuth.auth.currentUser; will be null
        * */
        const user = await this.afAuth.auth.currentUser;

        /*
        * we can convert `user` to boolean by using !!
        * */
        const isLoggedIn = !!user;

        if (!isLoggedIn) {
            this.snackService.authError();
        }

        return isLoggedIn;
    }
}
