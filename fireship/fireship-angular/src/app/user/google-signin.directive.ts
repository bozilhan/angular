import { Directive, HostListener } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import * as firebase from 'firebase/app';

@Directive({
  selector: '[appGoogleSignin]',
})
export class GoogleSigninDirective {
  constructor(private afAuth: AngularFireAuth) {}

  /*
  HostListener can listen to events on the DOM
  based on the element this directive is attached to

  method name should be on_EVENT_NAME that we handle in
  */
  @HostListener('click')
  onClick() {

  }
}
