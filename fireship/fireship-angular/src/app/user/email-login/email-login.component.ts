import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'app-email-login',
  templateUrl: './email-login.component.html',
  styleUrls: ['./email-login.component.scss'],
})
export class EmailLoginComponent implements OnInit {
  /*
  FromGroup is a collection of inputs that all get tight together
  and validated together with rxjs being the underline glue that allows that to happen in reactive way gets updated on every keypress
  */
  form: FormGroup;

  /*
  to determine which backend method that we invoke with firebase depending on what user is trying to do from the frontend
  */
  type: 'login' | 'signup' | 'reset' = 'signup';

  loading = false;

  /*
  it  will be in most cases error messages from firebase if the authentication method fails
  for whatever reason
  */
  serverMessage: string;

  /*
  then the component is initialized is actually built the reactive form
  which we do with the FormBuilder service
  */
  constructor(private fb: FormBuilder, private afAuth: AngularFireAuth) {}

  ngOnInit() {
    /*
    we call formBuilderGroup which takes as object with the form configuration
    so each property in this object represents a form field in the form

    each property takes an array
    the first element of that array begin default value which in most cases would be empty string if you working with text field
    the second element in the array is another array that takes a set of validators
    so you can validate this property as required or you might validate that has a proper email format
    or you can validate with your own custom regex
    or you can even build your custom async validator
    */
    this.form = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.minLength(6), Validators.required]],
      /*
      we have to implement custom validator
      */
      passwordConfirm: ['', []],
    });
  }

  /*
  to change the type of the form that the user is trying to use
  like signup, login or password reset forms
  */
  changeType(type) {
    this.type = type;
  }

  /*
  we can use with these values with ngIf in the template
  you conditionally show different elements based on the type of form
  that the user is filling out
  */
  get isLogin() {
    return this.type === 'login';
  }

  get isSignup() {
    return this.type === 'signup';
  }

  get isPasswordReset() {
    return this.type === 'reset';
  }

  /*
  to get actual form controls from the reactive form
  the reason we setup this getters is because each individual form control would be used frequently in template
  if we didnt set them up we would have to call form get with the form control name and becomes super verbose in the template
  */
  get email() {
    return this.form.get('email');
  }

  get password() {
    return this.form.get('password');
  }

  get passwordConfirm() {
    return this.form.get('passwordConfirm');
  }

  get passwordDoesMatch() {
    if (this.type !== 'signup') {
      return true;
    } else {
      return this.password.value === this.passwordConfirm.value;
    }
  }

  /*
  onSubmit will do the backend work when the form is submitted
  this will be fired when the user submits the form
  */
  async onSubmit() {
    this.loading = true;

    const email = this.email.value;
    const password = this.password.value;

    /*
     if any of the backend calls fail we want to show the error message to the user in the UI
    */
    try {
      if (this.isLogin) {
        await this.afAuth.signInWithEmailAndPassword(email, password);
      }

      if (this.isSignup) {
        await this.afAuth.createUserWithEmailAndPassword(email, password);
      }

      if (this.isPasswordReset) {
        await this.afAuth.sendPasswordResetEmail(email, password);
        this.serverMessage = 'Check your email';
      }
    } catch (err) {
      this.serverMessage = err;
    }

    this.loading = false;
  }
}
