/* 
GENERAL IDEA OF HOW ASYNCHRONOUS CODE WORKS
simulate an example on how it would be for you to get some data back from a server
*/
console.log('Start');

/* 


Callbacks
callback is just a function that is passed in as a parameter 
that is going to run later on in a later time
*/
const loginUser = (email, password, myCallback) => {
    /* 
    when we login the user we want to get back the email

    while using setTimeout we simulate server's response time
    */
    setTimeout(() => {
        myCallback({ userEmail: email });
    }, 1500);
}
const getUserVideos = (email, clbck) => {
    setTimeout(() => {
        clbck(['video1', 'video2', 'video3'])
    }, 1000);
};

/* 
CALLBACK HELL
*/
const user = loginUser('a@q.com', 123456, user => {
    /* 
    when the setTimeout runs we invoke myCallback function
    which TRIGGERS ALL OF THE FUNCTIONALITY here
    */
    console.log(user);
    getUserVideos(user.userEmail, videos => {
        console.log(videos);
    })
});

console.log('End');

/*
output
Start
End
{userEmail:"a@q.com"}
['video1', 'video2', 'video3']
*/