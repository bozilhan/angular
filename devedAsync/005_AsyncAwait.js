const loginUser = (email, password) => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            console.log('now we have the data')
            resolve({ userEmail: email });
        }, 3000);
    });
}

const getUserVideos = (email) =>
    new Promise((resolve, reject) =>
        setTimeout(() => {
            console.log('vido')
            resolve(['video1', 'video2', 'video3'])
        }, 1000));

const videoDetails = video => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            console.log('video', video)
            resolve(['title of the video'])
        }, 2000);
    })
}

/* 
REFACTOR 
    loginUser('ed', 'bumba')
        .then(user => getUserVideos(user.email))
        .then(videos => videoDetails(videos[0]))
        .then(detail => console.log(detail));
to make it look like SYNCHRONOUS CODE


we need to explicitly tell javascript that this is going to be asynchronous code 
we just say `async` at the beginning of the function
*/
const displayUser = async () => {
    try {
        const loggedUser = await loginUser('ed', 13);
        const videos = await getUserVideos(loggedUser.userEmail);
        const detail = await videoDetails(videos[0]);
        console.log(detail);
    } catch (err) {
        console.log('we could not get the videos')
    }
}

displayUser();