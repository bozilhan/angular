/* 
promise
promise is just an object that basically gives us back either a result of an asynchronous operation or a failure of a asynchronous operation

promise allows us to  FIX CALLBACK HELL

resolve(SUCCESS) -> if we get data back successfully then do something with it
reject(PROBLEM) -> if we dont get it back successfully then I dont know display error message or do something with it

CREATION OF PROMISE
*/

const promise = new Promise((resolve, reject) => {
    setTimeout(() => {
        /* 
        if you successfully got the data back you would run resolve
        */
        resolve({ user: 'ed' });
    }, 2000);
});

const promiseReject = new Promise((resolve, reject) => {
    setTimeout(() => {
        reject(new Error('user not logged in'));
    }, 2000);
});

/* 
CONSUMPTION OF PROMISE

in .then() we have the information so the result here
*/
promise.then(user => console.log(user));

promiseReject
    .then(user => console.log(user))
    .catch(err => console.log(err.message));