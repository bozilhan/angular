console.log('Start');

const loginUser = (email, password) => {
    /* 
    we return a promise 
    we DONT use callback which exist in 001_SimulateAsyncCode.js anymore
    */
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            console.log('now we have the data')
            resolve({ userEmail: email });
        }, 3000);
    });
}

const getUserVideos = (email) =>
    new Promise((resolve, reject) =>
        setTimeout(() => {
            console.log('vido')
            resolve(['video1', 'video2', 'video3'])
        }, 1000));

const videoDetails = video => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            console.log('video', video)
            resolve(['title of the video'])
        }, 2000);
    })
}

/* 
CONSUMPTION

we are getting make the user information and we are waiting for the user information ONCE we GET user information back THEN we are DOING SOMETHING with the videos and THEN we are doing something with the `detail` 

we HAVE TO WAIT FOR ONE ONE SPECIFIC THING TO START EXECUTING THE OTHER THING

we have to wait for 3 seconds for loginUser to get back the data
we have to wait for 1 seconds for getUserVideos to get back the data
we have to wait for 2 seconds for videoDetails to get back the data
*/
loginUser('ed', 'bumba')
    .then(user => getUserVideos(user.email))
    .then(videos => videoDetails(videos[0]))
    .then(detail => console.log(detail));

console.log('End');

/* 
OUTPUT
Start
End
now we have the data
vido
video video1
{"title of the video"}
*/