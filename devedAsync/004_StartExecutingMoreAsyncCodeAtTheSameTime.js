/* 
ASYNC AWAIT
it still uses promises but it is just a nicer and easier way for us to write asynchronous code
it doesnt really change in the background it is still going to use promises but it is just an easy and simple syntax for us to write it

what if I want to get some data from youtube or facebook but I want to kind of RUN THEM AT THE SAME TIME

I DONT WANT TO HAVE TO WAIT FOR ONE ONE SPECIFIC THING TO START EXECUTING THE OTHER THING

START GETTING DATA FROM YOUTUBE AND FACEBOOK AT THE SAME TIME
*/

const yt = new Promise(resolve => {
    setTimeout(() => {
        console.log('getting stuff from youtube')
        resolve({ videos: [1, 2, 3, 4, 5] })
    }, 5000);
});

const fb = new Promise(resolve => {
    setTimeout(() => {
        console.log('getting stuff from facebook')
        resolve({ user: 'Name' })
    }, 500);
});

/* 
If I want to run these at the same time and I want to get back both information 
what we can use we can use promise.all() we pass in array 

RHS of `const yt` starts getting the data as soon as RHS of `const fb` starts getting the data as well

so the both kind of(her ikisi de) RHS of `const yt` and RHS of `const fb` start running almost at the same time 

we dont have to wait for `yt` to get the data and then start fetching for `fb` 

so they kind of both(her ikisi de) execute at the same time

if one of them take longer the result is not going to come back UNTIL BOTH OF these promises are fulfilled both of them return us both of them resolved basically
*/  
Promise.all([yt, fb])
    .then(result => console.log(result));

/* 
OUTPUT
Promise {<pending>}
getting stuff from facebook 
getting stuff from youtube
(2) [{…}, {…}]
    0: {videos: Array(5)}
    1: {user: "Name"}
*/