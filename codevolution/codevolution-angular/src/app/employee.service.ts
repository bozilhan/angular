import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor() {
  }

  getEmployees(): any {
    return [
      {id: 1, name: 'Andrew', age: 30},
      {id: 1, name: 'Andrew', age: 30},
      {id: 1, name: 'Andrew', age: 30},
      {id: 1, name: 'Andrew', age: 30},
    ];
  }
}
