import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-pipes',
  templateUrl: './pipes.component.html',
  styleUrls: ['./pipes.component.css']
})
export class PipesComponent implements OnInit {
  name: string = 'codevolution';
  message: string = 'Welcome to codevolution';
  person: any = {
    firstName: 'John',
    lastName: 'Barz'
  };

  date: Date = new Date();

  ngOnInit(): void {
  }

}
