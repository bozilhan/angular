import {Component, OnInit} from '@angular/core';

/*
to use built-in validations we need to make use of the validators class import Validators

FormArray class makes it possible to maintain a dynamic list of controls
*/
import {FormGroup, FormControl, FormArray, Validators} from '@angular/forms';
import {FormBuilder} from '@angular/forms';
import {ForbiddenNameValidator, forbiddenNameValidator} from './shared/user-name.validator';
import {PasswordValidator} from './shared/password.validator';
import {RegistrationService} from './registration.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
    /*
    FormGroup instance that represents the user registration form
    */
    registrationForm: FormGroup;

    /*
      WITHOUT FormBuilder service
      registrationForm = new FormGroup({
      userName: new FormControl('Vishwas'),
      password: new FormControl(''),
      confirmPassword: new FormControl(''),
      address: new FormGroup({
        city: new FormControl(''),
        state: new FormControl(''),
        postalCode: new FormControl('')
      })
    });
    */
    constructor(
        private fb: FormBuilder,
        private registrationService: RegistrationService) { }

    ngOnInit() {
        /*
        generate form controls

        to create form group we call group() method

        the group() method takes in an object with the different form controls as keys

        */
        this.registrationForm = this.fb.group(
            {
                userName: [
                    /*
                    0th index v  alue is default value
                    */
                    '',
                    /*
                    second element in the array is where you specify the validation rules
                    for the form control
                    */
                    [
                        Validators.required,
                        Validators.minLength(3),
                        forbiddenNameValidator,
                        ForbiddenNameValidator(/password/)
                    ]
                ],
                password: [''],
                confirmPassword: [''],
                email: [''],
                subscribe: [false],

                /*
                address is the another form group
                so AGAIN we need to call the group() method
                on the the formbuilder instance
                */
                address: this.fb.group({
                    city: [''],
                    state: [''],
                    postalCode: ['']
                }),
                /*
                * Define formarray in the form model
                * */
                alternateEmails: this.fb.array([])
            },
            /*
            PasswordValidator is on the form group and not the form control

            second argument to the group() method is the validator
            */
            {validator: PasswordValidator}
        );

        /*
        this.registrationForm.get('subscribe').valueChanges returns an observable
        */
        this.registrationForm.get('subscribe').valueChanges
            .subscribe(checkedValue => {
                const email = this.registrationForm.get('email');
                if (checkedValue) {
                    email.setValidators(Validators.required);
                } else {
                    email.clearValidators();
                }
                email.updateValueAndValidity();
            });
    }

    /*
    getters that return a form control
    in doing so you can see that the html looks a lot more cleaner and readable
    just a personal preference and is by no means mandatory
    */
    get userName() {
        return this.registrationForm.get('userName');
    }

    get email() {
        return this.registrationForm.get('email');
    }

    /*
    to access this form array easily in the html
    we are going to type assert it to FormArray
    * */
    get alternateEmails() {
        return this.registrationForm.get('alternateEmails') as FormArray;
    }

    /*
    every time this method is called a form control is pushed into the formarray
    */
    addAlternateEmail() {
        this.alternateEmails.push(this.fb.control(''));
    }

    /*
    to set different form control values

    setValue() and patchValue() are very useful when you have to fill form values from an REST API or a service

    setValue() is used when you have to set values to all the form controls
    patchValue() can be used when you have to set values to few of the form controls
    */
    loadAPIData() {
        /*
          setValue is very strict about maintaining the structure of the form group
          you HAVE TO PASS ALL TH E FORM CONTROL VALUES

          this.registrationForm.setValue({
          userName: 'Bruce',
          password: 'test',
          confirmPassword: 'test',
          address: {
            city: 'City',
            state: 'State',
            postalCode: '123456'
          }
        });
        */

        /*
        if you do have to set values for ONLY FEW of the fields
        you can make use of the patchValue() method
        */
        this.registrationForm.patchValue({
            userName: 'Bruce',
            password: 'test',
            confirmPassword: 'test'
        });
    }

    onSubmit() {
        console.log(this.registrationForm.value);

        this.registrationService.register(this.registrationForm.value)
            .subscribe(response => console.log('Success!', response),
                error => console.error('Error!', error)
            );
    }
}
