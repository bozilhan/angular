import { ValidatorFn, AbstractControl } from '@angular/forms';

/*
the function accepts one parameter which is the form control being validated

validation function returns either of two values
when the validation fails it returns an object where the key is of type string and
the value is of type any
if validation passed it returns null

drawback of a validator function is that it can accept only one parameter which is the form control
so we cannot simply pass in a second parameter
*/
export function forbiddenNameValidator(control: AbstractControl): { [key: string]: any } | null {
  /*
  in the function body we first test if the form control value matches the pattern

  if the username contains the string admin we set the forbidden flag to true else we set it to false
  */
  const forbidden = /admin/.test(control.value);

  /*
  based on thr forbidden flag we can return either the object or null
  so if forbidden we return the error as forbidden name along with the form control value as the value property
  */
  return forbidden ? { 'forbiddenName': { value: control.value } } : null;
}


/*
instead what we have to do is create a factory function that accepts a string as a parameter
and returns the validator function itself

FACTORY FUNCTION
pass parameter to custom validators

that accepts a parameter of type regEx which is the pattern we have to validate against

this function returns a Validator Function so the return type is validator function
*/
export function ForbiddenNameValidator(forbiddenName: RegExp): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } | null => {
    const forbidden = forbiddenName.test(control.value);
    return forbidden ? { 'forbiddenName': { value: control.value } } : null;
  };
}
