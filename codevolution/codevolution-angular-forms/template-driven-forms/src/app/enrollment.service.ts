import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { User } from './user';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EnrollmentService {

  _url = 'http://localhost:3000/enroll';
  constructor(private _http: HttpClient) { }

  enroll(user: User) {
    /*
    post request will return the response as an observable
    */
    return this._http.post<any>(this._url, user)
      /*
      we will be catching the error from the server and then
      TROWING it to the SUBSCRIBED COMPONENT

      we need the help of rxjs
      */
      .pipe(catchError(this.errorHandler))
  }

  errorHandler(error: HttpErrorResponse) {
    return throwError(error)
  }
}
