/*
typescript compiler generates a public field
for each public constructor parameter and
AUTOMATICALLY ASSIGNS the parameters value to that field
when you create a new user
*/
export class User {
    constructor(
        public name?: string,
        public email?: string,
        public phone?: number,
        public topic?: string,
        public timePreference?: string,
        public subscribe?: boolean
    ) {}
}
