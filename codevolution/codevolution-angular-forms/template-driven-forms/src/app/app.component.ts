import { Component } from '@angular/core';
import { User } from './user';
import { EnrollmentService } from './enrollment.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  topics = ['Angular', 'React', 'Vue'];

  /*
  by having this instance of the model
  it is now possible to bind the user model data to our enrollment form
  */
  userModel = new User('Rob', 'rob@test.com', 5556665566, 'default', 'morning', true);
  topicHasError = true;

  /*
    it is ADVISABLE TO DISABLE or HIDE the submit button ONCE it has been clicked

    that is because it might CAUSE SERIOUS PROBLEMS WHEN MAKING PAYMENTS
    or CHECKING OUT on e-commerce sites for example

    when form is submitted we simply hide the entire form
    that gives appearance of the form being submitted as well
  */
  submitted = false;
  errorMsg = '';

  constructor(private _enrollmentService: EnrollmentService) {}

  validateTopic(value) {
    if (value === 'default') {
      this.topicHasError = true;
    } else {
      this.topicHasError = false;
    }
  }

  onSubmit(userForm) {
    console.log(userForm);
    this.submitted = true;
    this._enrollmentService.enroll(this.userModel)
      .subscribe(
        response => console.log('Success!', response),
        error => this.errorMsg = error.statusText
      )
  }
}
