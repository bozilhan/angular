/*
    FUNCTIONS

    : number { --> return type
*/
function add(num1: number, num2: number): number {
  return num1 + num2;
}

/*
    OPTIONAL parameters MUST BE AFtER THE REQUIRED parameters
    c++ da oldugu gibi
*/
function addWithOptionalParameter(num1: number, num2?: number): number {
  // if num2 is truty value
  if (num2) {
    return num1 + num2;
  }
  return num1;
}
/*
   second parameter is treated as undefined 
*/
addWithOptionalParameter(5);

function addWithDefaultParameter(num1: number, num2: number = 10): number {
  return num1 + num2;
}

/*
    metoda gecen objectin propertyleri metod argumanindakilerle AYNI OLMALI
    aksi takdirde hata aliriz
    const t = {
        a: 'Bruce',
        b: 'Wayne'
        };

    fullName(t); --> HATA metoda gecilen objectin propertyleri a ve b
*/
function fullName(person: {firstName: string; lastName: string}) {
  console.log(`${person.firstName} ${person.lastName}`);
}

const p = {
  firstName: 'Bruce',
  lastName: 'Wayne'
};

fullName(p);

