/*
    two syntax to declare array
*/
const strArr1: string[] = ['hello', 'world'];
const strArr2: Array<string> = ['hello', 'world'];

/*
    array to any
    it can hold ANY value
*/
const anyArr: any[] = ['hello', 10, true];

/*
    TUPLES
    tuples which are SPECIAL arrays that let you SPECIFY the TYPE
    the array can contain
    the number of element are FIXED
    ORDER of the VALUES HAS TO MATCH ORDER of the TYPES!!!

    FIXED number of values with DIFFERENT TYPES tuples are the way to go
*/

/*
    when we specify the [] we can specify the TYPE that are for this particular array
    for this example I want only string and number
    string and number are ONLY 2 type that SHOULD BE VALID

    myTuple indicates that the array CONTAINS EXACTLY ONE string value and ONE NUMERIC value

    we can not add other numeric value to myTuple
    ERROR!!!
    const myTuple: [string, number] = ['hi', 10, 21];
    const myTuple: [string, number] = [22, 'chris'];
    
*/
const myTuple: [string, number] = ['chris', 22];
console.log(myTuple[0]); // hi
console.log(myTuple[1]); // 10
