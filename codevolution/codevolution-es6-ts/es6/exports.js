/*
    https://www.youtube.com/watch?v=jpFAvYV-8tM&list=PLC3y8-rFHvwhI0V5mE9Vu6Nm-nap8EcjV&index=26
    NAMED Export

    import statements are HOISTED

    imports are READ-ONLY

    moduleB.js den import ettigimiz herhangi bir degiskenin STATEINI DEGISTIREMEYIZ. Hata aliriz!!!

    we can CHANGE PROPERTIES of OBJECT

    in named export the name of variable HAS TO MATCH imported variable!!!
*/

/********************************************
    BEGIN moduleB.js
********************************************/
//due to export keyword fname is ready for to be accessed in OTHER MODULES
export const fname = 'Chandler';
export const lname = 'Bing';

// if you have 100 variables
const fname = 'Chandler';
const lname = 'Bing';
const obj = {
  name: 'Joey'
};

// specify 100 variables separated by comma
export {fname, lname, obj};
/********************************************
    END moduleB.js
********************************************
********************************************/

/********************************************
*********************************************
    BEGIN moduleA.js
********************************************/
// fname and lname can be imported but can NEVER be CHANGED
import {fname, lname, obj} from './moduleB.js';
console.log(`${fname} ${lname}`); // Chandler Bing

// HATA ALIRIZ
fname = 'Ross';

// we can CHANGE PROPERTIES of imported OBJECT
obj.name = 'Ross';

// we can specify an alias while importing
// when you use an ALIAS for an import
// you MUST USE THAT ALIAS!!!!
import {fname as f, lname as l} from './moduleB.js';
console.log(`${fname} ${lname}`); // Uncaught (in promise) ReferenceError: fname is not defined
console.log(`${f} ${l}`); // Chandler Bing

/********************************************
    END moduleA.js
********************************************/

/*
    DEFAULT EXPORTS
    
    whenever we have modules that export ONLY a SINGLE VALUE or a FUNCTION 
    then we can make use of the DEFAULT keyword while EXPORTING

    we DONT HAVE TO USE curly braces while importing DEFAULT EXPORT
    in default import and export the NAME DOEST HAVE TO MATCH
    because we are going to be exporting ONLY a SINGLE VALUE from moduleB.js 
    and that is going to be captured in WHATEVER we are importing

    we can also provide ALIAS for importing default but only thing is 
    we NEED to USE CURLY BRACES {}
*/
/********************************************
    BEGIN moduleB.js
********************************************/
const fname = 'Chandler';

// since fname is going to be the ONLY export from this particular file or module
export default fname;
/********************************************
    END moduleB.js
********************************************
********************************************/

/********************************************
*********************************************
    BEGIN moduleA.js
********************************************/
// we DONT HAVE TO USE curly braces while importing DEFAULT EXPORT
// in default import and export the NAME DOEST HAVE TO MATCH
// firstName is going to mapped onto fname
import firsName from './moduleB.js';
console.log(`${firsName}`); // Chandler

// we can also provide ALIAS for importing default but only thing is
// we NEED to USE CURLY BRACES {}
import {default as f} from './moduleB.js';
console.log(`${f}`); // Chandler

/********************************************
    END moduleA.js
********************************************/

/*
    EXPORTING FUNCTIONS AND CLASSES
*/

/********************************************
    BEGIN moduleB.js
********************************************/
export const greet = message => {
  console.log(message);
};

export class GreetMessage {
  greetFromClass() {
    console.log('greetFromClass');
  }
}

/********************************************
    END moduleB.js
********************************************
********************************************/

/********************************************
*********************************************
    BEGIN moduleA.js
********************************************/
import {greet, GreetMessage} from './moduleB.js';
greet('Chandler'); // Chandler

const gm = new GreetMessage();
gm.greet() // greetFromClass
/********************************************
    END moduleA.js
********************************************/
