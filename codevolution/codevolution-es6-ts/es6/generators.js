/*
    https://www.youtube.com/watch?v=8n8ASL1pPt0&list=PLC3y8-rFHvwhI0V5mE9Vu6Nm-nap8EcjV&index=40
    GENERATORS
    once functions start executing they ALWAYS RUN to completion before any other code can run
    however generator isa SPECIAL type of function 
    which can be PAUSED IN THE MIDDLE OF EXECUTION RUN SOME OTHER CODE and then RESUME THE SAME FUNCTION
    FROM WHERE WE LEFT OFF
    this pausing of execution is possible with the help of a new keyword known as YIELD

    generator is a SPECIAL FUNCTION capable of pausing and resuming execution with the help of 
    yield keyword


    WHAT TO USE
    generators use to simplify our code when we write our CUSTOM ITERATORS
*/

/*
  this is generator
  first we yield the value 1 
  and then the execution is going to pause
  generator is going to RESUME its execution when we again tell it to continue its execution
  so then generator executes console.log('After 1st yield'); and yield 2;
  EVERY EXECUTION it is going to hit the NEXT YIELD POINT
*/
function* createGenerator() {
  yield 1;
  console.log('After 1st yield');
  yield 2;
}

/*
    this provides us with the REFERENCE
    it is NOT going to START EXECUTING this function
    to INVOKE this function we need to call NEXT() method on the generator

    OUTPUT
    1. console.log(gen.next());   
        {value: 1, done: false}
    2. console.log(gen.next()); 
        After 1st yield
        {value: 2, done: false}
    3. console.log(gen.next()); 
        {value: undefined, done: true} there are NO MORE YIELD done is set to true
*/
const gen = createGenerator();
console.log(gen.next());
console.log(gen.next());
console.log(gen.next());
