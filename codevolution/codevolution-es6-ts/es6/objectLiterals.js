/*
    https://www.youtube.com/watch?v=fgoNcYHxfdM&list=PLC3y8-rFHvwhI0V5mE9Vu6Nm-nap8EcjV&index=15
    OBJECT LITERALS
    */

const firstname = 'Chandler';
const lastname = 'Bings';

/*
    when the PROPERTY name is SAME AS the VARIABLE name we can reduce variable name
    EQUIVALENT
    const person={
        firstname: firstname,
        lastname: lastname
    }
    */
   const person = {
       firstname,
       lastname
    };
    
 /*
    EQUIVALENT
    const createPerson = (firstName, lastName, age) => {
    const fullName = firstName + ' ' + lastName;
    return {
        firstName,
        lastName,
        fullName,
        isSenior(){return age > 60}
  };
};
*/
const createPerson = (firstName, lastName, age) => {
  const fullName = firstName + ' ' + lastName;
  return {
    firstName,
    lastName,
    fullName,
    isSenior= () => age > 60
  };
};

const p = createPerson('Ross','Gellar', 32);
console.log(p.firstName);
console.log(p.lastName);
console.log(p.fullName);

/*
    isSenior () ile BIRLIKTE CAGIRILIR. FUNCTION/ARROW FUNCTION
    Bu ornekte console false basilir 32 > 60 FALSE
*/
console.log(p.isSenior());


/*
    key propertynin bosluklu string olma durumu
*/
const personWithSpace = {
    'first name':'Chandler'
}

/*
    we can also use variables AS key/property names
*/
const ln = 'last name';

const personWithSpaceAndVariable = {
    'first name':'Chandler',
    // we need to ENCLOSE(cevrelemek,kapsamak,cevirmek) it within SQUARE BRACKETS
    [ln]:'Bing'
}
console.log(personWithSpaceAndVariable['first name']); // Chandler
console.log(personWithSpaceAndVariable[[ln]]); // Bing
console.log(personWithSpaceAndVariable[ln]); // Bing
console.log(personWithSpaceAndVariable); // {first name: "Chandler", last name: "Bing"}


const personWithSpaceAndVariable = {
    'first name':'Chandler',
    // without SQUARE BRACKETS
    ln:'Bing'
}
console.log(personWithSpaceAndVariable['first name']); // Chandler
console.log(personWithSpaceAndVariable[[ln]]); // undefined
console.log(personWithSpaceAndVariable[ln]); // undefined
console.log(personWithSpaceAndVariable); // {first name: "Chandler", ln: "Bing"}