/*
   BEGIN EQUIVALENT
   (param1, param2, paramN) => EXPRESSION // implicit return

   (param1, param2, paramN) => {return EXPRESSION}

   function (param1, param2, paramN) {
      return EXPRESSION;
   }

   Object Literal
   => ({key1:value1, key2:value2})        // DISTA PARANTEZ ()
   => {return {key1:value1, key2:value2}) // DISTA CURLY BRACE {}

expressionlar deger donduren kod satirlaridir.
*/
const getRegValueFunction = function () {
  return 10;
};

const getArrowValue = () => {
  return 10;
};
/*
 return ile birlikte CURLY BRACE {} de KALKAR!!!
 aksi takdirde calismaz
 const getArrowValueII = () => {10}; // UNDEFINED

 metod VOID ise {} olup olmama durumu FARKETMEZ
 const withCurly = (m, bonus) => {console.log('bonus: ',m+bonus)}; // bonus: 3
 const withoutCurly = (m, bonus) => console.log('bonus: ',m+bonus); // bonus: 3
*/
const getArrowValueII = () => 10;
/*
   END EQUIVALENT
*/

/*
BEGIN OBJECT LITERAL`

SADECE SADECE SADECE
OBJECT dondururken arrow function bodysi PARANTEZLE BASLAR ()

Arrow functions,can be used to return an object literal expression.
The only caveat is that the body needs to be WRAPPED IN PARENTHESES,
in order to distinguish between a block and an object (both of which use curly brackets).
*/

const setNameIdsEs5 = function setNameIds(id, name) {
  return {
    id: id,
    name: name
  };
};

// DIS PARANTEZE DIKKAT () return YOK!!!
const setNameIdsEs6 = (id, name) => ({id: id, name: name});

// DIS PARANTEZE DIKKAT {} ve return
const setNameIdsEs6WithReturn = (id, name) => {
  return {id: id, name: name};
};

const setNameIdsEs5Result = setNameIdsEs5('barz', 5);
const setNameIdsEs6Result = setNameIdsEs6('barz', 6);
const setNameIdsEs6WithReturnResult = setNameIdsEs6WithReturn('barz', 66);

/*
END OBJECT LITERAL
*/

/*
 tek parametrede () kullanmaya gerek yok
 omit parentheses when you have SINGLE PARAMETER
*/
const getArrowValueOneParameter = m => {
  return 10 * m;
};

const getArrowValueOneParameterII = m => 10 * m;

const getArrowValueTwoParameters = (m, bonus) => 10 * m + bonus;

/*
 THIS KEYWORD
 https://www.youtube.com/watch?v=0T5M3agKEnk&list=PLC3y8-rFHvwhI0V5mE9Vu6Nm-nap8EcjV&index=11
*/

/*
 BEGIN DEFAULT PARAMETER
*/
const getValue = (value = 10, bonus = 5) => {
  console.log('value: ' + value + ' bonus: ' + bonus);
};

getValue(undefined, 3); // value: 10 bonus: 3
getValue(undefined, undefined); // value: 10 bonus: 5
getValue(null, 7); // value: null bonus: 7

const getValueII = (value = 10, bonus = value * 0.1) => {
  console.log('value: ' + value + ' bonus: ' + bonus);
};

getValueII(); // value: 10 bonus: 1
getValueII(20); // value: 20 bonus: 2
getValueII(20, 30); // value: 20 bonus: 30

/*
 0.1 magic number yerine const member variable tanimlanip ilgili degisken gecilebilir
 const let XXX = 0.1
 const getValueII = (value = 10, bonus = value * XXX) => {
*/
const percentBonus = () => 0.1;

/*
 arguments.length arrow functionda CALISMAZ!!!
 default values are NOT going to be considered as part of the ARGUMENT LIST

 function(value = bonus, bonus = 5 * percentBonus()) HATA VERIR SAGDAN SOLA OKUNMAZ!!!
*/
const getValueIII = function (value = 10, bonus = value * percentBonus()) {
  console.log(
    'value: ' +
      value +
      ' bonus: ' +
      bonus +
      ' arguments.length: ' +
      arguments.length
  );
};

getValueIII(); // value: 10 bonus: 1 arguments.length: 0
getValueIII(20); // value: 20 bonus: 2 arguments.length: 1
getValueIII(20, 30); // value: 20 bonus: 30 arguments.length: 2

/*
 END DEFAULT PARAMETER
*/
