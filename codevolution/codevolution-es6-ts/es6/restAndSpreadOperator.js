/*
    https://www.youtube.com/watch?v=TK0xDoQ4UKA&list=PLC3y8-rFHvwhI0V5mE9Vu6Nm-nap8EcjV&index=13
    rest parameter represents an INDEFINITE NUMBERS OF ARGUMENTS as an ARRAY
    ... converts list of function arguments into an array
    and then we can iterate that array
    rest --> combine
    rest operator is specified in the FUNCTION DECLARATION
    const displayColors = (message, ...colors) => { } REST operator NOT SPREAD operator!!!
*/
const displayColors = (message, ...colors) => {
  console.log(message);

  /*
    for ... in
    diziyi for...IN ile dolanirken key INDEX e tekabul eder
    diziyi for...OF ile dolanirken key ELEMANA e tekabul eder
  */
  for (const key in colors) {
    console.log(colors[key]);
  }
};

const message = 'List of Colors';
displayColors(message, 'Red');
displayColors(message, 'Red', 'Blue');
displayColors(message, 'Red', 'Blue', 'Green');

/*
   SPREAD!!!!
   https://www.youtube.com/watch?v=Fc6DPYx9aQU&list=PLC3y8-rFHvwhI0V5mE9Vu6Nm-nap8EcjV&index=14
   spread is opposite of rest operator
   spread operator takes an ARRAY and SPLITS it into the INDIVIDUAL ELEMENTS
   spread --> yayilmak, ayrimak
   spread operator is specified DURING FUNCTION CALL!!!
   displayColors(message, ...colorArray); --> SPREAD operator NOT REST operator!!!
*/

const colorArray = ['Orange', 'Yellow', 'Indigo'];
/*
    using the SPREAD operator array gets split into the INDIVIDUAL ELEMENTS
    and those individual elements are again COMBINED into a SINGLE ARRAY 
    and then for loop of displayColors works!!!
*/
displayColors(message, ...colorArray);
