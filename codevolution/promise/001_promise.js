/* 
Understanding JavaScript Promises in Simple English
https://www.youtube.com/watch?v=YiYtwbnPfkY
promise is simply an object in js. promise is always in one of the THREE STATES
1. pending which is initial state that is neither fulfilled nor rejected
2. fulfilled meaning that the operation completed successfully 
3. rejected meaning that the operation failed

why would you use promise
promises help us deal with asynchronous code in a far more simpler way compared to callbacks 

we can avoid callback hell with promises and the code can be sort of read in a simple synchronous way 

you CANNOT DIRECTLY MUTATE THE STATUS OF A PROMISE
you can call the result function to fulfill the promise or reject function to reject to promise

resolve() and reject() are typically called after an async operation 

CALLBACK FUNCTIONS
callback functions are functions that are passed in as arguments to other functions
the functions are passed in as arguments to other functions they are callback functions
*/

/*
HOW TO CREATE PROMISE
*/
const promise = new Promise();

/* 
HOW TO FULFILL OR REJECT THE PROMISE

resolve and reject are both functions

resolve is function which when called changes the status of the promise from `pending` to `fulfilled`
reject is function which when called changes the status of the promise from `pending` to `rejected`
*/
const promise = new Promise((resolve, reject) => {});

/* 
we are going to assume that for your friend to go out and text you back 
it takes 5 seconds 

so our code now changes to incorporate the setTimeout 
*/
const promise = new Promise((resolve, reject) => {
  setTimeout(() => {
    /* 
        if the food truck was found
        we will call resolve() after 5 seconds
        change status from `pending` to `fulfilled`
        */
    resolve('Bringing tacos');
  }, 5000);
});

const promiseReject = new Promise((resolve, reject) => {
  setTimeout(() => {
    /*
        if the food truck was not found
        we will call reject() after 5 seconds
        change status from `pending` to `rejected`
        */
    reject('not bringing tacos. Food truck not there');
  }, 5000);
});

/* 
HOW TO EXECUTE CALLBACK FUNCTIONS BASED ON THE STATUS CHANGE OF THE PROMISE
*/

/* 
onFulfillment is a function called if resolve is called after the async operation 

promise AUTOMATICALLY INJECTS the ARGUMENT PASSED TO RESOLVE AS THE ARGUMENT TO THE onFulfillment callback
*/
const onFulfillment = result => {
  /* 
    resolve() was called
    */
  console.log(result);
  console.log('set up the table to eat tacos');
};

/* 
onRejection is the function to be called if reject is called after the async operation

promise AUTOMATICALLY INJECTS the ARGUMENT PASSED TO REJECT AS THE ARGUMENT TO THE onRejection callback
*/
const onRejection = reject => {
  /* 
    reject() was called
    */
  console.log(reject);
  console.log('start cooking pasta');
};

/* 
when we create new promise using the promise constructor function 
the promise object gives us access to two methods or functions if you want to call it that
.then() and .catch()

if the status of the promise changes from pending to fulfilled by calling the resolve function the function that is passed to then function will automatically get invoked

if the status of the promise changes from pending to rejected by calling the reject function the function that is passed to catch function will automatically get invoked

Encouraged Approach
even if your onFulfillment callback throws an exception, it is caught and then you can handle that exception gracefully
*/
promise.then(onFulfillment);
promise.catch(onRejection);

/* 
.then() function

you can pass onRejection as a 2nd argument to then() function 
the code works exactly as before

onRejection callback handles error from ONLY the promise
however if your callback itself throws an error or exception, there is no code to handle that

it is NOT PREFERRED overusing(asiri kullanma) catch function
*/
promise.then(onFulfillment, onRejection);

/* 
chaining promises

both then() and catch() method return promises. this means then() and catch() methods can be chained in js

chaining can be done as many times as you want to which also solves the problem of CALLBACK HELL
*/
promise.then(onFulfillment).catch(onRejection);

/* 
Promise.all() static method

you may want to QUERY MULTIPLE APIs and perform some actions but only AFTER ALL the APIs have FINISHED LOADING
*/
const promise1 = Promise.resolve(3); // immediately resolves
const promise2 = 42; // is not really promise
const promise3 = new Promise((resolve, reject) => {
  setTimeout(resolve, 100, 'foo'); // resolves after 100 milliseconds
});

/* 
you can pass in all the three promises as an array to Promise.all() 
you see the output as an array containing the results of the individual promises

HOWEVER EVEN IF ONE OF THE PROMISES REJECTS promise.all() WILL REJECT WITH THAT ERROR MESSAGE

the promise.all() method takes an iterable of promises as an input and returns a single promise that resolves to an array of the results of the input promises

returned promise will resolve when all of the input's promises have resolved, or if the input iterable contains no promises

it rejects immediately if any of the input promises reject or the non-promises throw an error, and will reject with this first rejection message/error

promise.all() returns even if one promise rejects
*/
Promise.all([promise1, promise2, promise3]).then(values => {
  console.log(values); // expected output: Array [3, 42, "foo"]
});

/* 
promise.allSettled()
which waits for all input promises to complete regardless of(gozetilmeksizin) whether or not one of them is rejected

promise.allSettled() returns AFTER all promises HAVE COMPLETED even one or more promises reject
*/
const promise1 = Promise.resolve(3); // immediately resolves
const promise2 = 42; // is not really promise
const promise3 = new Promise((resolve, reject) => {
  setTimeout(resolve, 100, 'foo'); // resolves after 100 milliseconds
});
Promise.allSettled([promise1, promise2, promise3]).then(values => {
  console.log(values); // expected output: Array [3, 42, "foo"]
});

/* 
promise.race()
returns a promise that fulfills or rejects as soon as one of the input promises fulfills or rejects with the value or reason from that promise
*/
const promise1 = new Promise((resolve, reject) => {
  setTimeout(resolve, 500, 'one');
});

const promise2 = new Promise((resolve, reject) => {
  setTimeout(resolve, 100, 'two');
});

Promise.race([promise1, promise2]).then(value => {
  console.log(value);
  // Both resolve, but promise2 is faster
  // expected output: "two"
});
