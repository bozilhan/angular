/* 
ASYNC AWAIT

there is a way to IMPROVE PROMISE even further(bundan da ote) that is by using the async/await keywords

async await keywords allow us to write COMPLETELY SYNCHRONOUS LOOKING CODE WHILE PERFORMING ASYNCHRONOUS TASK behind the scenes

the async and await keywords enable asynchronous, promise-based behavior to be written in a cleaner style, avoiding the need to explicitly configure promise chains

async-await introduced in es2017

async keyword returns a Promise
await keyword pauses execution till the Promise is resolved or rejected
*/

/* 
ASYNC 
async keyword is used to declare async functions

async functions are functions that are instances of the AsyncFunction constructor 

what is special about async functions
unlike normal functions async functions ALWAYS RETURN A PROMISE
*/

/* 
if you run this function in the browser console you are going to see 

browser console
Promise {<fulfilled>: "Hello"}
__proto__: Promise
[[PromiseState]]: "fulfilled"
[[PromiseResult]]: "Hello"

if you dont explicitly return a promise the async function will AUTOMATICALLY wrap the value in a RESOLVED PROMISE
*/
const greet = async () => {
  return 'Hello';
};

const greetRewrittenOutputWouldBeTheSame = async () => {
  return Promise.resolve('Hello');
};

/* 
in order to actually consume the the string value when the promise fulfills we would use .then() on the promise instance

browser console
Hello
*/
greet.then(value => console.log(value));

/* 
async keywords ensures that the function returns a promise but it is NOT JUST THAT
the real advantage of async functions become evident(belirgin) when you combine it with `await` keyword

await
await keyword can be put in front of any async promise based function to PAUSE YOUR CODE UNTIL THAT PROMISE SETTLES AND RETURNS ITS RESULT 

in simple terms you can say that the `await` operator MAKES JS WAIT UNTIL THE PROMISE SETTLES AND RETURNS A RESULT

await only works inside async functions. you CANNOT use `await` in NORMAL FUNCTIONS
*/

/* 
`await` keyword basically(temelde) PAUSES code execution till the promise settles
in this example the promise takes 1 second to settle. so after a second execution resumes in `Hello` is logged to console

it is IMPORTANT TO NOTE HERE that the `await` keyword literally(kelimenin tam anlamiyla) suspends(askiya almak) the greet() function execution until the promise settles and then resumes it with the promise return value

JS engine can do other tasks in the meantime(bu arada) as far as greet() function is concerned there is 1 second suspension where no further code will execute
*/
const greet = async () => {
  /* 
  we create a promise that resolves after 1 second  
  */
  const promise = new Promise((resolve, reject) => {
    setTimeout(() => resolve('Hello'), 1000);
  });

  /* 
  we await the promise and assign returned value to the result variable
  wait until promise resolves
  */
  let result = await promise;

  /* 
  prints Hello
   */
  console.log(result);
};

/* 
SEQUENTIAL vs CONCURRENT vs PARALLEL EXECUTION
*/
const resolveHello = () => {
  return new Promise(resolve => setTimeout(() => resolve('Hello'), 2000));
};

const resolveWorld = () => {
  return new Promise(resolve => setTimeout(() => resolve('World'), 1000));
};

/* 
SEQUENTIAL EXECUTION
*/
const sequentialStart = async () => {
  /* 
    resolveHello() will take 2 seconds and then `Hello` is logged to the console
    */
  const hello = await resolveHello();
  console.log(hello); // logs after 2 seconds

  /* 
    only then(ondan sonra) the execution goes to resolveWorld() which takes an additional second so after 3 seconds which is 2 + 1 the string `World` is logged to the console
    
    !!!
    if SECOND function is NOT DEPENDENT ON the FIRST function you probably SHOULD NOT BE DOING THIS AS THERE IS AN UNNECESSARY DELAY OF 1 SECOND

    total time taken to log `Hello` `World` in sequentialStart() is 3 seconds
    */
  const world = await resolveWorld();
  console.log(world); // logs after 2 + 1 = 3 seconds
};

/* 
CONCURRENT EXECUTION
*/
const concurrentStart = async () => {
  const hello = resolveHello();
  const world = resolveWorld();

  /*
  when logging to the console we AWAIT for the PROMISE to be FULFILLED

  what happens in doing so is hello gets resolved after 2 seconds and gets logged to the console since world actually resolves in just one second by the time hello is resolved world is ready with its value

  as soon as the execution comes to await world on line 135 it logs the value to the console IMMEDIATELY

  there is NO NEED TO WAIT in ADDITIONAL SECOND

  THIS IS PROBABLY WHAT YOU WANT TO BE DOING WHEN LOADING PARTS OF A PAGE

  concurrently fire off all your requests and then display the ui as per(gore) your requirement by AWAITING IN THE RIGHT ORDER
  
  total time taken to log `Hello` `World` in concurrentStart() is 2 seconds or in other words the longest time taken by a function to resolve
   */
  console.log(await hello); // Logs after 2 seconds
  console.log(await world); // logs after 2 seconds
};

/* 
PARALLEL EXECUTION

if you prefer that individual functions are resolved WITHOUT HAVING TO WAIT FOR OTHER FUNCTIONS TO BE RESOLVED you can make use of promise.all() and use async functions as arguments

this is the case of running WHATEVER CODE RESOLVES FIRST
*/
const parallel = () => {
  /* 
    in this example after 1 second `World` will be logged to the console and after 2 seconds TOTAL `Hello` will be logged to the console

    the output in this case would be `World` `Hello` and the total time taken of 2 seconds
    */
  Promise.all([
    (async () => console.log(await resolveHello()))(), // logs after 2 seconds
    (async () => console.log(await resolveWorld()))() // logs after 1 second
  ]);
};

/* 
in parallel function itself if you want to ensure execution is paused at promise.all() then on line 167 add `async` keyword and on line 168 you need to `await` promise.all() 

resulting output is World Hello Finally

js waits for ALL THE PROMISES TO BE RESOLVED BEFORE MOVING ON TO LINE 176
*/
const parallel = async () => {
  await Promise.all([
    (async () => console.log(await resolveHello()))(), // logs after 2 seconds
    (async () => console.log(await resolveWorld()))() // logs after 1 second
  ]);
  console.log('Finally'); // logged after World Hello
};

/* 
Define a function called sleep which accepts a duration parameter
The sleep function should suspend execution of the function it is invoked in 

sleep function accepts the duration parameter and it returns a new Promise
we use setTimeout to resolve the promise after the given duration
*/
const sleep = duration => {
  return new Promise(resolve => setTimeout(resolve, duration));
};

/* 
inside an async function we can use the sleep() function to pause execution
*/
const main = async () => {
  console.log('Logs immediately');
  await sleep(2000);
  console.log('Logs after 2 seconds');
};
