/****************************************************************************
 ** @license
 ** This file is part of yFiles for HTML 2.1.0.6.
 ** 
 ** yWorks proprietary/confidential. Use is subject to license terms.
 **
 ** Copyright (c) 2019 by yWorks GmbH, Vor dem Kreuzberg 28, 
 ** 72070 Tuebingen, Germany. All rights reserved.
 **
 ***************************************************************************/
(function(r){(function(f) {if("function"===typeof define&&define.amd) {define(['./impl/lang','./impl/core-lib','./impl/graph-binding','./impl/graph-core','./impl/graph-styles-core','./impl/graph-styles-default','./impl/graph-styles-other','./impl/graph-styles-template'],f);} else if("object"===typeof exports&&"undefined"!==typeof module&&"object"===typeof module.exports) {module.exports = f(require('./impl/lang'),require('./impl/core-lib'),require('./impl/graph-core'),require('./impl/graph-binding'),require('./impl/graph-styles-core'),require('./impl/graph-styles-default'),require('./impl/graph-styles-other'),require('./impl/graph-styles-template'));} else {f(r.yfiles.lang,r.yfiles);}}(function(lang,yfiles) {return yfiles;}));})("undefined"!==typeof window?window:"undefined"!==typeof global?global:"undefined"!==typeof self?self:this);
