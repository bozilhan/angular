/****************************************************************************
 ** @license
 ** This file is part of yFiles for HTML 2.1.0.6.
 ** 
 ** yWorks proprietary/confidential. Use is subject to license terms.
 **
 ** Copyright (c) 2019 by yWorks GmbH, Vor dem Kreuzberg 28, 
 ** 72070 Tuebingen, Germany. All rights reserved.
 **
 ***************************************************************************/
(function(r){(function(f) {if("function"===typeof define&&define.amd) {define(['./impl/lang'],f);} else if("object"===typeof exports&&"undefined"!==typeof module&&"object"===typeof module.exports) {module.exports = f(require('./impl/lang'));} else {f(r.yfiles.lang);}}(function(lang) {return lang;}));})("undefined"!==typeof window?window:"undefined"!==typeof global?global:"undefined"!==typeof self?self:this);
