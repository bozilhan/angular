/****************************************************************************
 ** @license
 ** This file is part of yFiles for HTML 2.1.0.6.
 ** 
 ** yWorks proprietary/confidential. Use is subject to license terms.
 **
 ** Copyright (c) 2019 by yWorks GmbH, Vor dem Kreuzberg 28, 
 ** 72070 Tuebingen, Germany. All rights reserved.
 **
 ***************************************************************************/
(function(r){(function(f) {if("function"===typeof define&&define.amd) {define(['./impl/lang','./impl/core-lib','./algorithms','./layout-organic','./router-other','./router-polyline','./impl/algorithms','./impl/layout-core','./impl/layout-router','./impl/layout-polyline','./impl/layout-organic','./impl/layout-multipage'],f);} else if("object"===typeof exports&&"undefined"!==typeof module&&"object"===typeof module.exports) {module.exports = f(require('./impl/lang'),require('./impl/core-lib'),require('./algorithms'),require('./layout-organic'),require('./router-other'),require('./router-polyline'),require('./impl/algorithms'),require('./impl/layout-core'),require('./impl/layout-router'),require('./impl/layout-polyline'),require('./impl/layout-organic'),require('./impl/layout-multipage'));} else {f(r.yfiles.lang,r.yfiles);}}(function(lang,yfiles) {return yfiles;}));})("undefined"!==typeof window?window:"undefined"!==typeof global?global:"undefined"!==typeof self?self:this);
