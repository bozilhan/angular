/****************************************************************************
 ** @license
 ** This file is part of yFiles for HTML 2.1.0.6.
 ** 
 ** yWorks proprietary/confidential. Use is subject to license terms.
 **
 ** Copyright (c) 2019 by yWorks GmbH, Vor dem Kreuzberg 28, 
 ** 72070 Tuebingen, Germany. All rights reserved.
 **
 ***************************************************************************/
(function(r){(function(f) {if("function"===typeof define&&define.amd) {define(['./impl/lang','./impl/core-lib','./algorithms','./layout-orthogonal','./router-other','./router-polyline','./layout-tree','./impl/algorithms','./impl/layout-core','./impl/layout-tree','./impl/layout-router','./impl/layout-polyline','./impl/layout-orthogonal','./impl/layout-orthogonal-compact'],f);} else if("object"===typeof exports&&"undefined"!==typeof module&&"object"===typeof module.exports) {module.exports = f(require('./impl/lang'),require('./impl/core-lib'),require('./algorithms'),require('./layout-orthogonal'),require('./router-other'),require('./router-polyline'),require('./layout-tree'),require('./impl/algorithms'),require('./impl/layout-core'),require('./impl/layout-tree'),require('./impl/layout-router'),require('./impl/layout-polyline'),require('./impl/layout-orthogonal'),require('./impl/layout-orthogonal-compact'));} else {f(r.yfiles.lang,r.yfiles);}}(function(lang,yfiles) {return yfiles;}));})("undefined"!==typeof window?window:"undefined"!==typeof global?global:"undefined"!==typeof self?self:this);
