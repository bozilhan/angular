/****************************************************************************
 ** @license
 ** This demo file is part of yFiles for HTML 2.1.0.6.
 ** Copyright (c) 2000-2018 by yWorks GmbH, Vor dem Kreuzberg 28,
 ** 72070 Tuebingen, Germany. All rights reserved.
 **
 ** yFiles demo files exhibit yFiles for HTML functionalities. Any redistribution
 ** of demo files in source code or binary form, with or without
 ** modification, is not permitted.
 **
 ** Owners of a valid software license for a yFiles for HTML version that this
 ** demo is shipped with are allowed to use the demo source code as basis
 ** for their own yFiles for HTML powered applications. Use of such programs is
 ** governed by the rights and conditions as set out in the yFiles for HTML
 ** license agreement.
 **
 ** THIS SOFTWARE IS PROVIDED ''AS IS'' AND ANY EXPRESS OR IMPLIED
 ** WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 ** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 ** NO EVENT SHALL yWorks BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 ** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 ** TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 ** PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 ** LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 ** NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 ** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 **
 ***************************************************************************/
(function(r){(function(f){if("function"==typeof define&&define.amd){define(['yfiles/lang'],function(lang){f(lang.yfiles);});}else{f(r.yfiles||(r.yfiles={}));}}(/*@yjs:keep*/
  function(yfiles) {
    yfiles.license = {
	  "date": "2/8/2019",
	  "distribution": true,
	  "domains": [ "*" ],
	  "fileSystemAllowed": true,
	  "licensefileversion": "1.1",
	  "localhost": false,
	  "oobAllowed": true,
	  "package": "complete",
	  "product": "yFiles for HTML",
	  "type": "unlimited",
	  "version": "2.1",
	  "watermark": "",
	  "key": "8ad614fc650267f617b318532aa7402dfe5bb9ed"
	}
	/* 
	// With localhost=true
	yfiles.license = {
	  "date": "2/8/2019",
	  "distribution": true,
	  "domains": [ "*" ],
	  "fileSystemAllowed": true,
	  "licensefileversion": "1.1",
	  "localhost": true,
	  "oobAllowed": true,
	  "package": "complete",
	  "product": "yFiles for HTML",
	  "type": "unlimited",
	  "version": "2.1",
	  "watermark": "",
	  "key": "9254ef71eb7500c0f45f0f9a43d107762060ae5f"
	}
	*/
  }));
}("undefined"!=typeof window?window:"undefined"!=typeof global?global:"undefined"!=typeof self?self:this));
