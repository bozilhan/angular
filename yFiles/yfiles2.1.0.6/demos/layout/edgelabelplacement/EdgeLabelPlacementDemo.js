/****************************************************************************
 ** @license
 ** This demo file is part of yFiles for HTML 2.1.0.6.
 ** Copyright (c) 2000-2019 by yWorks GmbH, Vor dem Kreuzberg 28,
 ** 72070 Tuebingen, Germany. All rights reserved.
 **
 ** yFiles demo files exhibit yFiles for HTML functionalities. Any redistribution
 ** of demo files in source code or binary form, with or without
 ** modification, is not permitted.
 **
 ** Owners of a valid software license for a yFiles for HTML version that this
 ** demo is shipped with are allowed to use the demo source code as basis
 ** for their own yFiles for HTML powered applications. Use of such programs is
 ** governed by the rights and conditions as set out in the yFiles for HTML
 ** license agreement.
 **
 ** THIS SOFTWARE IS PROVIDED ''AS IS'' AND ANY EXPRESS OR IMPLIED
 ** WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 ** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 ** NO EVENT SHALL yWorks BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 ** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 ** TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 ** PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 ** LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 ** NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 ** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 **
 ***************************************************************************/
'use strict'

require.config({
  paths: {
    yfiles: '../../../lib/umd/yfiles/',
    utils: '../../utils/',
    resources: '../../resources/'
  }
})
require([
  'yfiles/view-editor',
  'resources/demo-app',
  'resources/demo-styles',
  'yfiles/view-graphml',
  'yfiles/view-layout-bridge',
  'yfiles/layout-tree',
  'yfiles/layout-hierarchic',
  'yfiles/layout-orthogonal',
  'yfiles/router-polyline',
  'resources/license'
], (/** @type {yfiles_namespace} */ /** typeof yfiles */ yfiles, app, DemoStyles) => {
  /**
   * The graph component.
   * @type {yfiles.view.GraphComponent}
   */
  let graphComponent = null

  /**
   * The graph that contains the labels.
   * @type {yfiles.graph.IGraph}
   */
  let graph = null

  /**
   * The mapper which provides a PreferredPlacementDescriptor for each edge label.
   * This mapper is used by the layout algorithms to consider the preferred placement for the edge labels.
   * @type {yfiles.collections.IMapper.<yfiles.graph.ILabel,yfiles.layout.PreferredPlacementDescriptor>}
   */
  let descriptorMapper = null

  /**
   * Flag to prevent re-entrant layouts.
   * @type {boolean}
   */
  let layouting = false

  /**
   * This demo shows how to influence the placement of edge labels by a generic labeling algorithm as well as by a
   * layout algorithm with integrated edge labeling using a PreferredPlacementDescriptor.
   */
  function run() {
    graphComponent = new yfiles.view.GraphComponent('graphComponent')
    graph = graphComponent.graph

    registerCommands()

    initializeGraph()
    initializeStyles()
    initializeInputMode()

    initializeOptions()
    initializeLayoutComboBox()

    createSampleGraph()

    app.show(graphComponent)
  }

  /**
   * Does the label placement using the selected labeling/layout algorithm. Before this, the
   * PreferredPlacementDescriptor of the labels is set according to the option handlers settings.
   * @see {@link #updateLabelProperties}
   * @param {boolean} fitViewToContent Whether to animate the viewport
   */
  function doLayout(fitViewToContent) {
    if (!layouting) {
      layouting = true
      setUIDisabled(true)

      // retrieve current labeling/layout algorithm from the combo-box
      const layoutComboBox = document.getElementById('layoutComboBox')
      const layoutAlgorithm = layoutComboBox.options[layoutComboBox.selectedIndex].myValue

      // initialize layout executor
      const layout = new yfiles.layout.MinimumNodeSizeStage(
        new yfiles.layout.FixNodeLayoutStage(layoutAlgorithm)
      )
      const layoutExecutor = new yfiles.layout.LayoutExecutor(graphComponent, layout)
      layoutExecutor.duration = '0.5s'
      layoutExecutor.animateViewport = fitViewToContent

      // apply layout
      layoutExecutor
        .start()
        .then(() => {
          layouting = false
          setUIDisabled(false)
        })
        .catch(error => {
          layouting = false
          setUIDisabled(false)
          if (typeof window.reportError === 'function') {
            window.reportError(error)
          }
        })
    }
  }

  /**
   * Disables the HTML elements of the UI and the input mode.
   *
   * @param disabled true if the elements should be disabled, false otherwise
   */
  function setUIDisabled(disabled) {
    document.getElementById('layoutButton').disabled = disabled
    document.getElementById('layoutComboBox').disabled = disabled
  }

  /**
   * Called whenever a property of the option handler has changed.
   * @param source The HTMLElement that reported a change.
   */
  function onLabelPropertyChanged(source) {
    // updateLabelValues(getAffectedLabels(), source)
    // doLayout(false)
  }





  /**
   * Retrieves the index for the given option in the combo-box.
   * @param comboBox The combo-box that contains the given option.
   * @param value The value of the option for which the index is needed.
   * @return {number}
   */
  function getIndex(comboBox, value) {
    const options = comboBox.options
    for (let i = 0; i < options.length; i++) {
      const option = options[i]
      if (option.myValue === value) {
        return i
      }
    }
    return -1
  }

  /**
   * Binds the commands to the buttons of the toolbar and the input elements of the option handler.
   */
  function registerCommands() {
    const iCommand = yfiles.input.ICommand
    app.bindCommand("button[data-command='Undo']", iCommand.UNDO, graphComponent)
    app.bindCommand("button[data-command='Redo']", iCommand.REDO, graphComponent)
    app.bindCommand("button[data-command='FitContent']", iCommand.FIT_GRAPH_BOUNDS, graphComponent)
    app.bindCommand("button[data-command='ZoomIn']", iCommand.INCREASE_ZOOM, graphComponent)
    app.bindCommand("button[data-command='ZoomOut']", iCommand.DECREASE_ZOOM, graphComponent)
    app.bindCommand("button[data-command='ZoomOriginal']", iCommand.ZOOM, graphComponent, 1.0)

    app.bindAction("button[data-command='Layout']", () => doLayout(true))
    app.bindChangeListener("select[data-command='SelectLayout']", () => doLayout(true))
    app.bindAction("button[data-command='PlacementAlongEdgeChanged']", () =>
      onLabelPropertyChanged(document.getElementById('placementAlongEdgeChanged'))
    )
    app.bindAction("button[data-command='PlacementSideOfEdgeChanged']", () =>
      onLabelPropertyChanged(document.getElementById('placementSideOfEdgeComboBox'))
    )
    app.bindAction("button[data-command='SideReferenceChanged']", () =>
      onLabelPropertyChanged(document.getElementById('sideReferenceComboBox'))
    )
    app.bindAction("button[data-command='AngleReferenceChanged']", () =>
      onLabelPropertyChanged(document.getElementById('angleReferenceComboBox'))
    )
    app.bindAction("button[data-command='AngleRotationChanged']", () =>
      onLabelPropertyChanged(document.getElementById('angleRotationComboBox'))
    )
    document.getElementById('distanceToEdgeNumberField').addEventListener(
      'change',
      input => {
        if (input.target.value > 200) {
          alert('Distance cannot be larger than 200.')
          input.target.value = -1
        }
      },
      false
    )
    document.getElementById('angleNumberField').addEventListener(
      'change',
      input => {
        const angle = input.target.value
        if (angle <= -360 || angle >= 360) {
          input.target.value = angle % 360
        }
      },
      false
    )
  }

  /**
   * Adds mappers and label edit listeners to the graph.
   */
  function initializeGraph() {
    // the mapper for the preferred placement information
    descriptorMapper = graph.mapperRegistry.createMapper(
      yfiles.graph.ILabel.$class,
      yfiles.layout.PreferredPlacementDescriptor.$class,
      yfiles.layout.LayoutGraphAdapter.EDGE_LABEL_LAYOUT_PREFERRED_PLACEMENT_DESCRIPTOR_DP_KEY
    )

    // fix node port stage is used to keep the bounding box of the graph in the view port
    graph.mapperRegistry.createConstantMapper(
      yfiles.layout.FixNodeLayoutStage.FIXED_NODE_DP_KEY,
      true
    )

    // // add preferred placement information to each new label
    graph.addLabelAddedListener((source, event) => {
      descriptorMapper.set(event.item, new yfiles.layout.PreferredPlacementDescriptor())
    })
    // graph.addLabelRemovedListener((source, event) => {
    //   descriptorMapper.delete(event.item)
    // })

    graph.undoEngineEnabled = true
  }

  /**
   * Initializes node, edge and label styles that are applied when the graph is created.
   */
  function initializeStyles() {
    graph.nodeDefaults.style = new DemoStyles.DemoNodeStyle()
    graph.nodeDefaults.size = new yfiles.geometry.Size(50, 30)

    graph.edgeDefaults.style = new DemoStyles.DemoEdgeStyle()
    graph.edgeDefaults.labels.layoutParameter = yfiles.graph.FreeEdgeLabelModel.INSTANCE.createDefaultParameter()

    // the label style indicates the label's borders and does not auto-flip to show the actual placement
    graph.edgeDefaults.labels.style = new yfiles.styles.DefaultLabelStyle({
      backgroundStroke: 'lightblue',
      backgroundFill: 'rgba(255, 255, 255, 128)',
      autoFlip: false
    })
  }

  /**
   * Configures the input mode such that only labels can be added/removed to the graph.
   * Also, selecting labels will trigger an update of the option handler.
   */
  function initializeInputMode() {
    const inputMode = new yfiles.input.GraphEditorInputMode({
      allowCreateEdge: false,
      allowCreateNode: false,
      allowCreateBend: false,
      deletableItems: yfiles.graph.GraphItemTypes.LABEL,
      marqueeSelectableItems: yfiles.graph.GraphItemTypes.LABEL,
      selectableItems: yfiles.graph.GraphItemTypes.LABEL | yfiles.graph.GraphItemTypes.EDGE,
      focusableItems: yfiles.graph.GraphItemTypes.NONE
    })

    // update the option handler settings when the selection changes
    // inputMode.addMultiSelectionFinishedListener(() => updateLabelProperties(getAffectedLabels()))

    graphComponent.inputMode = inputMode
  }

  /**
   * Initializes the properties in the option handler with options and default values.
   */
  function initializeOptions() {
  }

  /**
   * Initializes the layout combo-box with a selection of layout algorithms.
   */
  function initializeLayoutComboBox() {
    const layoutComboBox = document.getElementById('layoutComboBox')
    addOption(
      layoutComboBox,
      'Hierarchic, Top to Bottom',
      createHierarchicLayout(yfiles.layout.LayoutOrientation.TOP_TO_BOTTOM)
    )

    layoutComboBox.selectedIndex = 0
  }

  /**
   * Creates a configured HierarchicLayout.
   * @param {yfiles.layout.LayoutOrientation} layoutOrientation
   * @return {yfiles.hierarchic.HierarchicLayout}
   */
  function createHierarchicLayout(layoutOrientation) {
    const layout = new yfiles.hierarchic.HierarchicLayout()
    layout.integratedEdgeLabeling = true
    layout.layoutOrientation = layoutOrientation

    disableAutoFlipping(layout)
    return layout
  }


  /**
   * Disables auto-flipping for labels on the layout algorithm since the result could differ from the values in the
   * PreferredPlacementDescriptor.
   * @param multiStageLayout The current layout algorithm.
   */
  function disableAutoFlipping(multiStageLayout) {
    const labelLayoutTranslator = multiStageLayout.labeling
    labelLayoutTranslator.autoFlipping = false
  }

  /**
   * @param {HTMLElement} comboBox
   * @param {string} text
   * @param {object} value
   */
  function addOption(comboBox, text, value) {
    const option = document.createElement('option')
    option.text = text
    option.myValue = value
    comboBox.add(option)
  }

  /**
   * Creates a graph with labels at each edge and an initial layout.
   */
  function createSampleGraph() {
    // create nodes
    const nodes = []
    for (let i = 0; i < 2; i++) {
      nodes.push(graph.createNode())
    }

    // graph.createEdge(nodes[0], nodes[1]);
    // graph.createEdge(nodes[0], nodes[1]);
    // graph.createEdge(nodes[0], nodes[1]);
    // graph.createEdge(nodes[0], nodes[1]);
    // graph.createEdge(nodes[0], nodes[1]);
    // graph.createEdge(nodes[0], nodes[1]);
    // graph.createEdge(nodes[0], nodes[1]);
    // graph.createEdge(nodes[0], nodes[1]);
    // graph.createEdge(nodes[0], nodes[1]);
    // graph.createEdge(nodes[0], nodes[1]);
    // graph.createEdge(nodes[0], nodes[1]);



    // create edges
    const edges = []
    edges.push(graph.createEdge(nodes[1], nodes[1]))
    edges.push(graph.createEdge(nodes[0], nodes[1]))
    edges.push(graph.createEdge(nodes[0], nodes[1]))

    
    edges.push(graph.createEdge(nodes[0], nodes[1]))
    edges.push(graph.createEdge(nodes[0], nodes[1]))
    edges.push(graph.createEdge(nodes[0], nodes[1]))

    edges.push(graph.createEdge(nodes[0], nodes[1]))
    edges.push(graph.createEdge(nodes[0], nodes[1]))
    edges.push(graph.createEdge(nodes[0], nodes[1]))

    edges.push(graph.createEdge(nodes[0], nodes[1]))
    edges.push(graph.createEdge(nodes[0], nodes[1]))

    edges.push(graph.createEdge(nodes[0], nodes[1]))
    edges.push(graph.createEdge(nodes[0], nodes[1]))
    edges.push(graph.createEdge(nodes[0], nodes[1]))



    // edges.push(graph.createEdge(nodes[0], nodes[4]))
    // edges.push(graph.createEdge(nodes[0], nodes[6]))
    // edges.push(graph.createEdge(nodes[0], nodes[6]))
    // edges.push(graph.createEdge(nodes[1], nodes[7]))
    // edges.push(graph.createEdge(nodes[1], nodes[9]))
    // edges.push(graph.createEdge(nodes[2], nodes[9]))
    // edges.push(graph.createEdge(nodes[4], nodes[10]))
    // edges.push(graph.createEdge(nodes[5], nodes[10]))
    // edges.push(graph.createEdge(nodes[5], nodes[11]))
    // edges.push(graph.createEdge(nodes[5], nodes[26]))
    // edges.push(graph.createEdge(nodes[6], nodes[12]))
    // edges.push(graph.createEdge(nodes[12], nodes[6]))
    // edges.push(graph.createEdge(nodes[6], nodes[13]))
    // edges.push(graph.createEdge(nodes[7], nodes[13]))
    // edges.push(graph.createEdge(nodes[8], nodes[13]))
    // edges.push(graph.createEdge(nodes[8], nodes[16]))
    // edges.push(graph.createEdge(nodes[9], nodes[17]))
    // edges.push(graph.createEdge(nodes[11], nodes[18]))
    // edges.push(graph.createEdge(nodes[13], nodes[19]))
    // edges.push(graph.createEdge(nodes[14], nodes[19]))
    // edges.push(graph.createEdge(nodes[15], nodes[19]))
    // edges.push(graph.createEdge(nodes[16], nodes[20]))
    // edges.push(graph.createEdge(nodes[16], nodes[21]))
    // edges.push(graph.createEdge(nodes[18], nodes[22]))
    // edges.push(graph.createEdge(nodes[19], nodes[23]))
    // edges.push(graph.createEdge(nodes[19], nodes[25]))
    // edges.push(graph.createEdge(nodes[22], nodes[26]))
    // edges.push(graph.createEdge(nodes[23], nodes[27]))
    // edges.push(graph.createEdge(nodes[24], nodes[27]))
    // edges.push(graph.createEdge(nodes[24], nodes[28]))

    // add labels
    edges.forEach((edge, i) => {
      graph.addLabel(edge, 'Labellllllllllllllllll')
      // if (i === 8 || i === 29) {
      //   graph.addLabel(edge, 'Label')
      //   graph.addLabel(edge, 'Label')
      // }
    })

    // initial layout and label placement
    const layout = new yfiles.hierarchic.HierarchicLayout()
    layout.integratedEdgeLabeling = true
    const labeling = new yfiles.labeling.GenericLabeling()
    labeling.coreLayout = layout
    graph.applyLayout(labeling)
    graphComponent.fitGraphBounds()

    // update option handler
    // updateLabelProperties(graph.edgeLabels)
  }

  /**
   * Returns a collection of labels that are currently affected by option changes.
   * Affected labels are all selected labels or all labels in case no label is selected.
   * @return {yfiles.collections.IEnumerable.<yfiles.graph.ILabel>}
   */
  function getAffectedLabels() {
    const selectedLabels = graphComponent.selection.selectedLabels
    return selectedLabels.size > 0 ? selectedLabels : graph.edgeLabels
  }

  run()
})
