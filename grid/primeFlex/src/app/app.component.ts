import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'primeFlex';

  checked = true;

  onCumpleServicioSocial(checked: Event) {
    console.log('servicio updated', checked);
  }
}
